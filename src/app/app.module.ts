import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
//import { FormsModule } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './pages/components/components.module';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './pages/layouts/admin-layout/admin-layout.component';
import { MaterialModule} from './material/material.module';

// Componentes del home
import { HomeComponent } from './home/home.component';
import { HomeContactComponent } from './home/components/home-contact/home-contact.component';
import { HomeFooterComponent } from './home/components/home-footer/home-footer.component';
import { HomeInstructionComponent } from './home/components/home-instruction/home-instruction.component';
import { HomeNewslatterComponent } from './home/components/home-newslatter/home-newslatter.component';
import { HomeNavbarComponent } from './home/components/home-navbar/home-navbar.component';
import { HomeSliderComponent } from './home/components/home-slider/home-slider.component';
import { SearchBarComponent } from './home/components/search-bar/search-bar.component';
// Registro y login
import { RegistroClienteComponent } from './pages/registros/registro-cliente/registro-cliente.component';
import { LoginComponent } from './login/login/login.component';
import { VerComentarioDialogComponent } from './pages/admin-components/dialogs-admin/ver-comentario-dialog/ver-comentario-dialog.component';
import { ResponderComentarioDialogComponent } from './pages/admin-components/dialogs-admin/responder-comentario-dialog/responder-comentario-dialog.component';
import { CrearPlanMembresiaComponent } from './pages/admin-components/dialogs-admin/crear-plan-membresia/crear-plan-membresia.component';
import { EditarPlanMembresiaComponent } from './pages/admin-components/dialogs-admin/editar-plan-membresia/editar-plan-membresia.component';
import { CrearCategoriaComponent } from './pages/admin-components/dialogs-admin/crear-categoria/crear-categoria.component';
import { CrearSubcategoriaComponent } from './pages/admin-components/dialogs-admin/crear-subcategoria/crear-subcategoria.component';
import { EditarCategoriaComponent } from './pages/admin-components/dialogs-admin/editar-categoria/editar-categoria.component';
import { EditarUsuarioComponent } from './pages/admin-components/dialogs-admin/editar-usuario/editar-subcategoria.component';
import { EditarSubcategoriaComponent } from './pages/admin-components/dialogs-admin/editar-subcategoria/editar-subcategoria.component';
import { EditarDisputaComponent } from './pages/admin-components/dialogs-admin/editar-disputa/editar-disputa.component';
import { ServicioRequeridoComponent } from './pages/admin-components/dialogs-admin/servicio-requerido/servicio-requerido.component';
import { ServiciosRequeridosComponent } from './pages/admin-components/servicios-requeridos/servicios-requeridos.component';
// Componentes del proveedor
import { HomeProviderComponent } from './pages/provider-components/home-provider/home-provider.component';
import { ClientHomeComponent } from './pages/client-home/client-home.component';
import { ClientCardsProvidersComponent } from './pages/client-home/components/client-cards-providers/client-cards-providers.component';
import { SearchClientComponent } from './pages/client-home/components/search-client/search-client.component';
import { ClientNavbarComponent } from './pages/client-home/components/client-navbar/client-navbar.component';
import { PaquetesDeCreditoService } from './pages/services/paquetes-de-credito.service';
import { AceptarAlertComponent } from './pages/admin-components/dialogs-admin/aceptar-alert/aceptar-alert.component';
import { CrearBlogComponent } from './pages/admin-components/blogs-components/crear-blog/crear-blog.component';
import { EditarBlogComponent } from './pages/admin-components/blogs-components/editar-blog/editar-blog.component';

// Servicios David

import {LoginService} from './services/login.service'
import {RegisterService} from './services/register.service'
import {UsuarioService} from './services/usuarios.service';

import { ModalSubCategoriesComponent } from './dialogs/modal-sub-categories/modal-sub-categories.component';
import { SubcategoriesService } from './services/subcategories.service';
import { MapaService } from './pages/services/mapa.service';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    NgbModule,
    ToastrModule.forRoot(),
    MaterialModule,
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    RegistroClienteComponent,
    HomeComponent,
    HomeContactComponent,
    HomeFooterComponent,
    HomeInstructionComponent,
    HomeNewslatterComponent,
    HomeNavbarComponent,
    HomeSliderComponent,
    SearchBarComponent,
    LoginComponent,
    ClientHomeComponent,
    ClientCardsProvidersComponent,
    SearchClientComponent,
    ClientNavbarComponent,
    HomeProviderComponent,
    // dialogs,
    VerComentarioDialogComponent,
    ResponderComentarioDialogComponent,
    CrearPlanMembresiaComponent,
    EditarPlanMembresiaComponent,
    CrearCategoriaComponent,
    CrearSubcategoriaComponent,
    EditarCategoriaComponent,
    EditarSubcategoriaComponent,
    EditarDisputaComponent,
    ServicioRequeridoComponent,
    AceptarAlertComponent,
    HomeProviderComponent,
    ModalSubCategoriesComponent,
    CrearBlogComponent,
    EditarBlogComponent,
    EditarUsuarioComponent,
   // ProvClientsProfileComponent,
  ],
  providers: [LoginService, RegisterService,PaquetesDeCreditoService,SubcategoriesService , MapaService, UsuarioService ],
  bootstrap: [AppComponent],
  entryComponents:[
    VerComentarioDialogComponent,
    ResponderComentarioDialogComponent,
    CrearPlanMembresiaComponent,
    EditarPlanMembresiaComponent,
    EditarUsuarioComponent,
    CrearCategoriaComponent,
    CrearSubcategoriaComponent,
    EditarCategoriaComponent,
    EditarSubcategoriaComponent,
    EditarDisputaComponent,
    ServicioRequeridoComponent,
    //Dialog
    ModalSubCategoriesComponent,
    AceptarAlertComponent,
    CrearBlogComponent,
    EditarBlogComponent

  ]
})
export class AppModule { }
