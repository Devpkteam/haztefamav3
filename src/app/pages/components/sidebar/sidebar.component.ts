import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    id?: string;
    childrens?: any[] 
    client?: string// sirve si el menu es desplegable
    provider?: string
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'design_app', class: '' },
    { path: '/icons', title: 'Icons',  icon:'education_atom', class: '' },
    { path: '/maps', title: 'Maps',  icon:'location_map-big', class: '' },
    { path: '/notifications', title: 'Notifications',  icon:'ui-1_bell-53', class: '' },

    { path: '/user-profile', title: 'User Profile',  icon:'users_single-02', class: '' },
    { path: '/table-list', title: 'Table List',  icon:'design_bullet-list-67', class: '' },
    { path: '/typography', title: 'Typography',  icon:'text_caps-small', class: '' },
    { path: '/upgrade', title: 'Upgrade to PRO',  icon:'objects_spaceship', class: 'active active-pro' }

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})

export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(public router:Router) {
    this.role = localStorage.getItem('role')
  }

//Menu Administrador
  putDashboardAmin(){
    ROUTES.length = 0
    let configGeneralChildrens = [  // estos son los hijos de configuraciones, cada uno con un parentId que es el id del padre
      {path: 'admin/config-general/configuracion-general', title: '1) Configuraciones general', icon:'loader_gear', class:'', parentId:'gene'},
      {path: 'admin/config-general/cambiar-contrasena', title: '2) Cambiar contraseña', icon:'objects_key-25', class:'',parentId:'gene'},
      {path: 'admin/config-general/administrar-contenido', title: '3) Administrar contenido', icon:'design_bullet-list-67', class:'',parentId:'gene'},
      {path: 'admin/config-general/plantillas-correo', title: '4) Plantillas de correo', icon:'ui-1_email-85', class:'',parentId:'gene'},
      {path: 'admin/config-general/imagen-banner', title: '5) imagen de Banner', icon:'design_image', class:'',parentId:'gene'},
      {path: 'admin/config-general/gestion-comentarios', title: '6) Gestión de Comentarios', icon:'ui-2_chat-round', class:'',parentId:'gene'},

    ]

    let UsuariosChildrens = [
      {path: 'admin/config-usuarios/administrar-usuarios', title: '1) Administración de usuarios', icon:'users_circle-08', class:'',parentId:'user'},
      {path: 'admin/config-usuarios/paquetes-credito', title: '2) paquetes de crédito', icon:'business_money-coins', class:'',parentId:'user'},

    ]

    let categoríasChildrens = [
      {path: 'admin/config-categorias/categorias', title: '1) categorias', icon:'ui-2_settings-90', class:'',parentId:'categ'},
      {path: 'admin/config-categorias/subcategorias', title: '2) subcategorias', icon:'ui-2_settings-90', class:'',parentId:'categ'},

    ]

    let serviChildrens = [
      {path: 'admin/config-servicios/requeridos', title: '1) Servicios requeridos', icon:'design_app', class:'',parentId:'adminservi'},
      {path: 'admin/config-servicios/preguntas', title: '2) Gestionar preguntas', icon:'ui-2_chat-round', class:'',parentId:'adminservi'},

    ]

    let pagosChildrens = [
      {path: 'admin/config-pagos/creditos', title: '1) Pagos de crédito', icon:'business_money-coins', class:'',parentId:'pagos'},
      {path: 'admin/config-pagos/servicios', title: '2) Pagos de servicio', icon:'shopping_bag-16', class:'',parentId:'pagos'},

    ]

    let blogsChildrens = [
      {path: 'admin/config-blogs/administrar', title: '1) Administrar blogs', icon:'ui-1_settings-gear-63', class:'',parentId:'blogs'},
      {path: 'admin/config-blogs/comentarios', title: '2) Comentarios de blog', icon:'ui-2_chat-round', class:'',parentId:'blogs'},

    ]

    let boleChildrens = [
      {path: 'admin/config-boletines/administrar', title: '1) Administrar boletines', icon:'ui-1_settings-gear-63', class:'',parentId:'bole'},
      {path: 'admin/config-boletines/usuarios', title: '2) Usuarios suscritos', icon:'users_single-02', class:'',parentId:'bole'},

    ]

    let logo = [
      {path: '#', title: '', icon:'', class:'',parentId:'logout'},

    ]



    ROUTES.push(
      { path: 'admin/home', title: 'HOME',  icon:'business_chart-bar-32', class: '' },
      { path: '#',id: 'gene' , title: 'General', icon: 'loader_gear', class: '', childrens:configGeneralChildrens}, // se le asigna un id y se le pasan los hijos
      { path: '#', id:'user', title: 'Usuarios', icon: 'users_single-02', class: '', childrens:UsuariosChildrens},
      { path: '#',id:'categ', title: 'Administrar Categorías', icon: 'design_bullet-list-67', class: '',childrens:categoríasChildrens},
      { path: 'admin/config-disputas', title: 'Administrar Disputa', icon: 'design_vector', class: ''},
      { path: 'admin/config-comision-admin', title: 'Comisión Administrativa', icon: 'business_briefcase-24', class: ''},
      { path: '#', id:'adminservi', title: 'Administración de Servicios', icon: 'shopping_box', class: '',childrens:serviChildrens},
      { path: '#',id:'pagos', title: 'Pagos', icon: 'business_bank', class: '',childrens:pagosChildrens},
      { path: '#',id:'blogs', title: 'Blogs', icon: 'education_paper', class: '',childrens:blogsChildrens},
      { path: 'admin/config/como-funciona', title: '¿Cómo funciona?', icon: 'travel_info', class: ''},
      { path: '#',id:'bole', title: 'Administrar Boletines', icon: 'location_map-big', class: '',childrens:boleChildrens},
      { path: '#',id:'logout', title: 'cerrar sesion', icon: 'media-1_button-power', class: '',childrens:logo},


      )
  }

  show; // variable de control para el menu desplegable
  openListChilds(id){
    // recible un id
    console.log(id)
    if(this.show == id){ // si el id es el mismo que la variable show, significa que ya estaba desplegado, asi que se asigna un string vacio para cerrar el menu
      this.show = ''
    }else if(id == 'logout'){
      console.log('cerrar')
      localStorage.removeItem('role')
      this.router.navigateByUrl('/')
    }
    else {
      this.show = id // de lo contrario le asignamos el id que abrira el menu desplegable correcto
    }

  }


  //Menu Cliente
  putDashboardClient(){
    ROUTES.length = 0
    ROUTES.push(
      { path: '/login/client/profile', title: 'Mi perfil',  icon:'users_single-02', class: '' },
      { path: 'client/requirements', title: 'Mis requerimientos',  icon:'business_badge', class: '' },
      { path: 'client/categories', title: 'Explorar categorías:',  icon:'business_chart-bar-32', class: '' },
      { path: 'client/provider-favorite', title: 'Proveedor favorito',  icon:'business_chart-bar-32', class: '' },
      { path: 'client/password', title: 'Cambiar contraseña',  icon:'ui-1_lock-circle-open', class: '' },
      { path: 'client/notifications', title: 'Notificaciones',  icon:'design_bullet-list-67', class: '' },
      { path: '/home-provider', title: 'Home Proveedor',  icon:'design_bullet-list-67', class: '', client: 'yes'},
      )
  }

  changeRol(rol){
    localStorage.setItem('role',rol);
    console.log('qlq')
  }

  ngOnInit() {
    this.loadDashboard()

    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }

  role;
  //role = 'client'
  loadDashboard(){

    if(this.role == "admin"){
      this.putDashboardAmin()
    }else if (this.role == 'provider'){
      this.putDashboardProvider()
    }else if( this.role == "client"){
        console.log("****** Eres cliente *******");
        this.putDashboardClient();
              //  this.router.navigate(['/client/home']) //client/home   home-client

    }

  }

  isMobileMenu() {
      if ( window.innerWidth > 991) {
          return false;
      }
      return true;
  };

  putDashboardProvider(){
    ROUTES.length = 0
    ROUTES.push(
      { path: '/login/provider/profile', title: 'Perfil',  icon:'users_single-02', class: '' }, //
      { path: 'provider/requirements', title: 'Mis requerimientos',  icon:'business_badge', class: '' },
      { path: 'provider/quotes', title: 'Cotizaciones enviadas',  icon:'shopping_delivery-fast', class: '' },
      { path: 'provider/membership', title: 'Membresia',  icon:'education_paper', class: '' },
      { path: 'provider/payments', title: 'Mis pagos',  icon:'business_money-coins', class: '' },
      { path: 'provider/change-password', title: 'Cambiar contraseña',  icon:'ui-1_lock-circle-open', class: '' }, //
      { path: 'provider/notifications', title: 'Notificaciones',  icon:'design_bullet-list-67', class: '' },
      { path: '/home-client', title: 'Home Cliente',  icon:'design_bullet-list-67', class: '', provider: 'yes'},
    )
  }
}
