import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { ChartsModule } from 'ng2-charts';
import { MaterialModule } from '../../../material/material.module';

// Componentes proveedor
import { ProfileComponent } from '../../provider-components/profile/profile.component';
import { RequirementsComponent } from '../../provider-components/requirements/requirements.component';
import { QuotesComponent } from '../../provider-components/quotes/quotes.component';
import { MembershipComponent } from '../../provider-components/membership/membership.component';
import { PaymentsComponent } from '../../provider-components/payments/payments.component';
import { ChangePasswordComponent } from '../../provider-components/change-password/change-password.component';
import { NotificationsProviderComponent } from '../../provider-components/notifications-provider/notifications-provider.component';

//cliente
import { ClientProfileComponent } from '../../client-home/components/client-profile/client-profile.component';
import { ClientChangePasswordComponent } from '../../client-home/components/client-change-password/client-change-password.component';

import { RequirementsDetailsComponent } from '../../provider-components/dialogs/requirements-details/requirements-details.component';

import { ClientRequirementsComponent } from '../../client-home/components/client-requirements/client-requirements.component';
import { ClientCategoriesComponent } from '../../client-home/components/client-categories/client-categories.component';
import { ClientNotifComponent } from '../../client-home/components/client-notif/client-notif.component';
import { SearchClientDashboardComponent } from '../../client-home/components/search-client-dashboard/search-client-dashboard.component';
import { ProviderFavoriteComponent } from '../../client-home/components/provider-favorite/provider-favorite.component';

import { AdminHomeComponent } from '../../admin-components/admin-home/admin-home.component';
import { ConfiguracionGeneralComponent } from '../../admin-components/configuracion-general/configuracion-general.component';
import { ConfigurarContrasenaComponent } from '../../admin-components/configurar-contrasena/configurar-contrasena.component';
import { ConfigurarContenidoComponent } from '../../admin-components/configurar-contenido/configurar-contenido.component';
import { ConfigurarPlantillaCorreoComponent } from '../../admin-components/configurar-plantilla-correo/configurar-plantilla-correo.component';
import { ConfigurarImagenBannerComponent } from '../../admin-components/configurar-imagenBanner/configurar-imagenBanner.component';
import { ConfigurarGestionComentariosComponent } from '../../admin-components/configurar-gestion-comentarios/configurar-gestion-comentarios.component';
import { AdministracionDeUsuariosComponent } from '../../admin-components/administracion-de-usuarios/administracion-de-usuarios.component';
import { PaquetesDeCreditoComponent } from '../../admin-components/paquetes-de-credito/paquetes-de-credito.component';
import { VerComentarioDialogComponent } from '../../admin-components/dialogs-admin/ver-comentario-dialog/ver-comentario-dialog.component';
import { AdministrarSubcategoriaComponent } from '../../admin-components/administrar-subcategoria/administrar-subcategoria.component';
import { AdministrarCategoriaComponent } from '../../admin-components/administrar-categoria/administrar-categoria.component';
import { AdministrarDisputaComponent } from '../../admin-components/administrar-disputa/administrar-disputa.component';
import { ComisionAdminComponent } from '../../admin-components/comision-admin/comision-admin.component';
import { PagosCreditosComponent } from '../../admin-components/pagos-creditos/pagos-creditos.component';
import { PagosServiciosComponent } from '../../admin-components/pagos-servicios/pagos-servicios.component';
import { ServiciosRequeridosComponent } from '../../admin-components/servicios-requeridos/servicios-requeridos.component';
import { ServiciosPreguntasComponent } from '../../admin-components/servicios-preguntas/servicios-preguntas.component';
import { BoletinesUsuariosComponent } from '../../admin-components/boletines-usuarios/boletines-usuarios.component';
import { BoletinesAdministrarComponent } from '../../admin-components/boletines-administrar/boletines-administrar.component';
import { ComoFuncionaComponent } from '../../admin-components/como-funciona/como-funciona.component';
import { AdministrarComentariosBlogsComponent } from '../../admin-components/administrar-comentarios-blogs/administrar-comentarios-blogs.component';
import { AdministrarBlogsComponent } from '../../admin-components/administrar-blogs/administrar-blogs.component';
import { AdministrarCategoriasBlogComponent } from '../../admin-components/administrar-categorias-blog/administrar-categorias-blog.component';
import { ProvidersComponent } from '../../provider-components/providers/providers.component';
import { ProviderProfileComponent } from '../../provider-components/provider-profile/provider-profile.component';
import { ClientsFilterComponent } from '../../provider-components/clients-filter/clients-filter.component';
import { ProvClientsProfileComponent } from '../../provider-components/prov-clients-profile/prov-clients-profile.component';


//import { SubcategoriesService } from '../../../services/subcategories.service';
import { ListProvidersComponent } from '../../client-home/components/list-providers/list-providers.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    NgbModule,
    ToastrModule.forRoot(),
    MaterialModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    UpgradeComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    // componentes proveedores
    ProfileComponent,
    RequirementsComponent,
    QuotesComponent,
    MembershipComponent,
    PaymentsComponent,
    ChangePasswordComponent,
    NotificationsProviderComponent,
    ProvClientsProfileComponent,
    //componentes clientes
    ClientProfileComponent,
    ClientRequirementsComponent,
    ClientCategoriesComponent,
    ClientNotifComponent,
    SearchClientDashboardComponent,
    ProviderFavoriteComponent,
    ClientChangePasswordComponent,
    // componentes administrador
    AdminHomeComponent,
    ConfiguracionGeneralComponent,
    ConfigurarContrasenaComponent,
    ConfigurarContenidoComponent,
    ConfigurarPlantillaCorreoComponent,
    ConfigurarImagenBannerComponent,
    ConfigurarGestionComentariosComponent,
    AdministracionDeUsuariosComponent,
    PaquetesDeCreditoComponent,
    AdministrarSubcategoriaComponent,
    AdministrarCategoriaComponent,
    AdministrarDisputaComponent,
    ComisionAdminComponent,
    PagosCreditosComponent,
    PagosServiciosComponent,
    ServiciosRequeridosComponent,
    ServiciosPreguntasComponent,
    BoletinesUsuariosComponent,
    BoletinesAdministrarComponent,
    ComoFuncionaComponent,
    AdministrarComentariosBlogsComponent,
    AdministrarBlogsComponent,
    AdministrarCategoriasBlogComponent,
    RequirementsDetailsComponent,
    ProvidersComponent,
    ProviderProfileComponent,
    ClientsFilterComponent,

    ListProvidersComponent
  ],

 // providers: [  ],

  entryComponents:[
    RequirementsDetailsComponent
  ]

})

export class AdminLayoutModule {}
