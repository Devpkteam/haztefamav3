import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';

// Componentes proveedor
import { ProfileComponent } from '../../provider-components/profile/profile.component';
import { RequirementsComponent } from '../../provider-components/requirements/requirements.component';
import { QuotesComponent } from '../../provider-components/quotes/quotes.component';
import { MembershipComponent } from '../../provider-components/membership/membership.component';
import { PaymentsComponent } from '../../provider-components/payments/payments.component';
import { ChangePasswordComponent } from '../../provider-components/change-password/change-password.component';
import { NotificationsProviderComponent } from '../../provider-components/notifications-provider/notifications-provider.component';
import { AdminHomeComponent } from '../../admin-components/admin-home/admin-home.component';
import { ConfiguracionGeneralComponent } from '../../admin-components/configuracion-general/configuracion-general.component';
import { ConfigurarContrasenaComponent } from '../../admin-components/configurar-contrasena/configurar-contrasena.component';
import { ConfigurarContenidoComponent } from '../../admin-components/configurar-contenido/configurar-contenido.component';
import { ConfigurarPlantillaCorreoComponent } from '../../admin-components/configurar-plantilla-correo/configurar-plantilla-correo.component';
import { ConfigurarImagenBannerComponent } from '../../admin-components/configurar-imagenBanner/configurar-imagenBanner.component';
import { ConfigurarGestionComentariosComponent } from '../../admin-components/configurar-gestion-comentarios/configurar-gestion-comentarios.component';
import { AdministracionDeUsuariosComponent } from '../../admin-components/administracion-de-usuarios/administracion-de-usuarios.component';
import { PaquetesDeCreditoComponent } from '../../admin-components/paquetes-de-credito/paquetes-de-credito.component';
import { AdministrarCategoriaComponent } from '../../admin-components/administrar-categoria/administrar-categoria.component';
import { AdministrarSubcategoriaComponent } from '../../admin-components/administrar-subcategoria/administrar-subcategoria.component';
import { ComisionAdminComponent } from '../../admin-components/comision-admin/comision-admin.component';
import { PagosCreditosComponent } from '../../admin-components/pagos-creditos/pagos-creditos.component';
import { PagosServiciosComponent } from '../../admin-components/pagos-servicios/pagos-servicios.component';
import { ServiciosRequeridosComponent } from '../../admin-components/servicios-requeridos/servicios-requeridos.component';
import { ServiciosPreguntasComponent } from '../../admin-components/servicios-preguntas/servicios-preguntas.component';
import { AdministrarDisputaComponent } from '../../admin-components/administrar-disputa/administrar-disputa.component';
import { BoletinesAdministrarComponent } from '../../admin-components/boletines-administrar/boletines-administrar.component';
import { BoletinesUsuariosComponent } from '../../admin-components/boletines-usuarios/boletines-usuarios.component';
import { ComoFuncionaComponent } from '../../admin-components/como-funciona/como-funciona.component';
import { AdministrarBlogsComponent } from '../../admin-components/administrar-blogs/administrar-blogs.component';
import { AdministrarCategoriasBlogComponent } from '../../admin-components/administrar-categorias-blog/administrar-categorias-blog.component';
import { AdministrarComentariosBlogsComponent } from '../../admin-components/administrar-comentarios-blogs/administrar-comentarios-blogs.component';

//clientes
import { ClientProfileComponent } from '../../client-home/components/client-profile/client-profile.component';
import { ClientChangePasswordComponent } from '../../client-home/components/client-change-password/client-change-password.component';
import { ClientRequirementsComponent } from '../../client-home/components/client-requirements/client-requirements.component';
import { ClientCategoriesComponent } from '../../client-home/components/client-categories/client-categories.component';
import { ClientNotifComponent } from '../../client-home/components/client-notif/client-notif.component';
import { SearchClientDashboardComponent } from '../../client-home/components/search-client-dashboard/search-client-dashboard.component';
import { ProviderFavoriteComponent } from '../../client-home/components/provider-favorite/provider-favorite.component';
import { ProvidersComponent } from '../../provider-components/providers/providers.component';
import { ProviderProfileComponent } from '../../provider-components/provider-profile/provider-profile.component';

//import { ClientCardsProvidersComponent } from '../../client-home/components/client-cards-providers/client-cards-providers.component';
//import { SearchClientComponent } from '../../client-home/components/search-client/search-client.component';
import { ListProvidersComponent } from '../../client-home/components/list-providers/list-providers.component';

import { ClientsFilterComponent } from '../../provider-components/clients-filter/clients-filter.component';
import { ProvClientsProfileComponent } from '../../provider-components/prov-clients-profile/prov-clients-profile.component';


export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'table-list',     component: TableListComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'upgrade',        component: UpgradeComponent },
    // rutas administrador
    { path: 'admin/home',        component:  AdminHomeComponent},
    { path: 'admin/config-general/configuracion-general',component:  ConfiguracionGeneralComponent},
    { path: 'admin/config-general/cambiar-contrasena',component:  ConfigurarContrasenaComponent},
    { path: 'admin/config-general/administrar-contenido',component:  ConfigurarContenidoComponent},
    { path: 'admin/config-general/plantillas-correo', component: ConfigurarPlantillaCorreoComponent},
    { path: 'admin/config-general/imagen-banner', component: ConfigurarImagenBannerComponent},
    { path: 'admin/config-general/gestion-comentarios', component: ConfigurarGestionComentariosComponent},
    { path:'admin/config-usuarios/administrar-usuarios', component: AdministracionDeUsuariosComponent},
    { path:'admin/config-usuarios/paquetes-credito', component: PaquetesDeCreditoComponent},
    { path:'admin/config-categorias/categorias', component: AdministrarCategoriaComponent},
    { path:'admin/config-categorias/subcategorias', component: AdministrarSubcategoriaComponent},
    { path:'admin/config-disputas', component: AdministrarDisputaComponent},
    { path:'admin/config-comision-admin', component: ComisionAdminComponent},
    { path:'admin/config-pagos/creditos', component: PagosCreditosComponent},
    { path:'admin/config-pagos/servicios', component: PagosServiciosComponent},
    { path:'admin/config-servicios/requeridos', component: ServiciosRequeridosComponent},
    { path:'admin/config-servicios/preguntas', component: ServiciosPreguntasComponent},
    { path:'admin/config-boletines/administrar', component: BoletinesAdministrarComponent},
    { path:'admin/config-boletines/usuarios', component: BoletinesUsuariosComponent},
    { path:'admin/config/como-funciona', component: ComoFuncionaComponent},
    { path:'admin/config-blogs/administrar', component: AdministrarBlogsComponent},
    { path:'admin/config-blogs/categorias', component: AdministrarCategoriasBlogComponent},
    { path:'admin/config-blogs/comentarios', component: AdministrarComentariosBlogsComponent},
    // Rutas provedores
    { path: 'provider/profile', component: ProfileComponent},
    { path: 'provider/requirements', component: RequirementsComponent},
    { path: 'provider/quotes', component: QuotesComponent},
    { path: 'provider/membership', component: MembershipComponent},
    { path: 'provider/payments', component: PaymentsComponent},
    { path: 'provider/change-password', component: ChangePasswordComponent},
    { path: 'provider/notifications', component: NotificationsProviderComponent},
    { path: 'provider/clients-filter', component: ClientsFilterComponent },
    { path: 'provider/clients-filter/profile', component: ProvClientsProfileComponent },
    //Rutas clientes
    { path: 'client/profile', component: ClientProfileComponent },  //
    { path: 'client/requirements', component: ClientRequirementsComponent },
    { path: 'client/categories', component: ClientCategoriesComponent },
    { path: 'client/provider-favorite', component: ProviderFavoriteComponent },
    { path: 'client/password', component: ClientChangePasswordComponent },
    { path: 'client/notifications', component: ClientNotifComponent },
    { path: 'client/providers', component: ProvidersComponent },
    { path: 'client/providers/profile', component: ProviderProfileComponent },



];
