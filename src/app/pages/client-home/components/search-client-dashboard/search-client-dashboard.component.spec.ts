import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchClientDashboardComponent } from './search-client-dashboard.component';

describe('SearchClientDashboardComponent', () => {
  let component: SearchClientDashboardComponent;
  let fixture: ComponentFixture<SearchClientDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchClientDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchClientDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
