//import { Component, OnInit } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator, MatDialog} from '@angular/material';
import { RequirementsDetailsComponent } from '../../../provider-components/dialogs/requirements-details/requirements-details.component';


export interface PeriodicElement {
  position: number;
  name: string;
  place: string;
  category: string;
  subcategory: string;
  time: string;
  status:string;
  price: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', place: 'aqui', category:'categoria', subcategory:'subcateforia', time:'12:04', status:'activo', price:'2.500'},
  {position: 2, name: 'Helium', place: 'aqui', category:'categoria', subcategory:'subcateforia', time:'12:04', status:'activo', price:'2.500'},
  {position: 3, name: 'Lithium', place: 'aqui', category:'categoria', subcategory:'subcateforia', time:'12:04', status:'activo', price:'2.500'},
  {position: 4, name: 'Beryllium', place: 'aqui', category:'categoria', subcategory:'subcateforia', time:'12:04', status:'activo', price:'2.500'},
  {position: 5, name: 'Boron', place: 'aqui', category:'categoria', subcategory:'subcateforia', time:'12:04', status:'activo', price:'2.500'},
  {position: 6, name: 'Carbon', place: 'aqui', category:'categoria', subcategory:'subcateforia', time:'12:04', status:'activo', price:'2.500'},
  {position: 7, name: 'Nitrogen', place: 'aqui', category:'categoria', subcategory:'subcateforia', time:'12:04', status:'activo', price:'2.500'},
  {position: 8, name: 'Oxygen', place: 'aqui', category:'categoria', subcategory:'subcateforia', time:'12:04', status:'activo', price:'2.500'},
  {position: 9, name: 'Fluorine', place: 'aqui', category:'categoria', subcategory:'subcateforia', time:'12:04', status:'activo', price:'2.500'},
  {position: 10, name: 'Neon', place: 'aqui', category:'categoria', subcategory:'subcateforia', time:'12:04', status:'activo', price:'2.500'},
];

@Component({
  selector: 'app-client-requirements',
  templateUrl: './client-requirements.component.html',
  styleUrls: ['./client-requirements.component.scss']
})

export class ClientRequirementsComponent implements OnInit {

 /// displayedColumns: string[] = ['position', 'name', 'place', 'category', 'subcategory', 'time', 'status', 'price', 'options'];

  displayedColumns: string[] = ['position', 'requirements', 'providers' ,  'options'];
  dataSource = ELEMENT_DATA;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(RequirementsDetailsComponent, {
      width: '50%',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  /*constructor() { }

  ngOnInit() {
  }*/

}
