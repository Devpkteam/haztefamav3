//import { Component, OnInit } from '@angular/core';

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-client-change-password',
  templateUrl: './client-change-password.component.html',
  styleUrls: ['./client-change-password.component.scss']
})
export class ClientChangePasswordComponent implements OnInit {

 /* constructor() { }

  ngOnInit() {
  }  */
  changeForm: FormGroup;

  constructor(
    public form:FormBuilder,
  ) {
    this.changeForm = form.group({
      actualPassword: ['', Validators.required],
      password: ['', Validators.required],
      repassword: ['', Validators.required],
      remember: [false]
    });

  }

  ngOnInit() {
  }

  changePassword(){

  }

}
