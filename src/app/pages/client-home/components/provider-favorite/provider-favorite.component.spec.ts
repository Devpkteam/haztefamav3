import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderFavoriteComponent } from './provider-favorite.component';

describe('ProviderFavoriteComponent', () => {
  let component: ProviderFavoriteComponent;
  let fixture: ComponentFixture<ProviderFavoriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderFavoriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
