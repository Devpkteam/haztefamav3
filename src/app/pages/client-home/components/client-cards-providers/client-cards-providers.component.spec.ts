import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientCardsProvidersComponent } from './client-cards-providers.component';

describe('ClientCardsProvidersComponent', () => {
  let component: ClientCardsProvidersComponent;
  let fixture: ComponentFixture<ClientCardsProvidersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientCardsProvidersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientCardsProvidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
