import { Component, OnInit } from '@angular/core';//
import {MatDialog} from '@angular/material/dialog';
import { ModalSubCategoriesComponent } from '../../../../dialogs/modal-sub-categories/modal-sub-categories.component';
import { SubcategoriesService } from '../../../../services/subcategories.service';

@Component({
  selector: 'app-client-cards-providers',
  templateUrl: './client-cards-providers.component.html',
  styleUrls: ['./client-cards-providers.component.scss']
})

export class ClientCardsProvidersComponent implements OnInit {

  categories = []

  constructor( public dialog: MatDialog, public subServicie:SubcategoriesService ) {   }

  ngOnInit() {
  }

 public subCategories( e ){

    let category = e._elementRef.nativeElement.id ;

   console.log("Evento cliente" + category );

   console.log( "elemento objeto"  );
   console.log( e  );


   this.subServicie.get_subcategories( category ).length=0;

   let subcategories = this.subServicie.get_subcategories( category )

    //Envia datos al Dialog Modal
   this.showPopotSubcategories( category , category , subcategories);

  }

//Alert

  async showPopotSubcategories( titulo , name_category , subcategories ){

    const alertModal = this.dialog.open(ModalSubCategoriesComponent,{
      width: '400px',
      height: '250px',
      data: { perfil:"client" , titulo: titulo , category : name_category , subcategory : subcategories }

    })

    await alertModal.afterClosed().subscribe((result)=>{

      if(result){
         console.log( "result despues de cerrar" );
         console.log( result )
      }
    })
  }//


}


