import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientNotifComponent } from './client-notif.component';

describe('ClientNotifComponent', () => {
  let component: ClientNotifComponent;
  let fixture: ComponentFixture<ClientNotifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientNotifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientNotifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
