// import { Component, OnInit } from '@angular/core';

import { Component, OnInit } from '@angular/core';//
import {MatDialog} from '@angular/material/dialog';
import { ModalSubCategoriesComponent } from './../../dialogs/modal-sub-categories/modal-sub-categories.component';
import { SubcategoriesService } from './../../services/subcategories.service';

// dialogs/modal-sub-categories/modal-sub-categories.component



@Component({
  selector: 'app-client-home',
  templateUrl: './client-home.component.html',
  styleUrls: ['./client-home.component.scss']
})
export class ClientHomeComponent implements OnInit {

  constructor( public dialog: MatDialog, public subServicie:SubcategoriesService ) {   }

  ngOnInit() {
  }

  public subCategories( e ){


    let id_category = e._elementRef.nativeElement.id ;

    let category = e._elementRef.nativeElement.name ;

   console.log("Evento cliente" + category );

   console.log( "elemento objeto"  );
   console.log( e  );


   this.subServicie.get_subcategories( category ).length=0;

   let subcategories = this.subServicie.get_subcategories( category )

    //Envia datos al Dialog Modal
   this.showPopotSubcategories( category , category , subcategories);

  }


  async showPopotSubcategories( titulo , name_category , subcategories ){

    const alertModal = this.dialog.open(ModalSubCategoriesComponent,{
      width: '400px',
      height: '250px',
      data: { perfil:"client" , titulo: titulo , category : name_category , subcategory : subcategories }

    })

    await alertModal.afterClosed().subscribe((result)=>{

      if(result){
         console.log( "result despues de cerrar" );
         console.log( result )
      }
    })
  }//

}
