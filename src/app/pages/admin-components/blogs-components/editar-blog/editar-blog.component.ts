import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { MatSnackBar, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PaquetesDeCreditoService } from '../../../services/paquetes-de-credito.service';
import { CrearPlanMembresiaComponent } from '../../dialogs-admin/crear-plan-membresia/crear-plan-membresia.component';
import { CategoriaService } from '../../../services/categoria.service';
import { BlogService } from '../../../services/blog.service';


@Component({
  selector: 'app-editar-blog',
  templateUrl: './editar-blog.component.html',
  styleUrls: ['./editar-blog.component.scss']
})
export class EditarBlogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CrearPlanMembresiaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public fb:FormBuilder,
    public paquetesService:PaquetesDeCreditoService,
    public _snackBar:MatSnackBar,
    public categoriaService:CategoriaService,
    public blogService:BlogService
    ) {}

    editarBlogForm = this.fb.group({
      titulo: [this.data.titulo,[Validators.required]],
      idCategoria: [this.data.idCategoria._id,[Validators.required]]
    })

  onNoClick(): void {
    this.dialogRef.close();
  }

  categorias
  getCategorias(){
    this.categoriaService.getCategorias().subscribe((resp:any)=>{
      console.log(resp)
      if(resp.ok){
        this.categorias = resp.categorias
      }else{
        this._snackBar.open('Ocurrio un error al intentar obtener las categorias, intentalo denuevo','aceptar')
      }
    })
  }
  

  editarBlog(){
    this.blogService.updateBlog(this.data._id,this.editarBlogForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.dialogRef.close(true)
      }else{
        this._snackBar.open('Ocurrio un error al intentar modificar el blog, intentalo denuevo','aceptar')

      }
    })
     
  }
  ngOnInit() {
    this.getCategorias()
  }

}
