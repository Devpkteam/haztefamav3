import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { MatSnackBar, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CrearPlanMembresiaComponent } from '../../dialogs-admin/crear-plan-membresia/crear-plan-membresia.component';
import { BlogService } from '../../../services/blog.service';
import { CategoriaService } from '../../../services/categoria.service';

@Component({
  selector: 'app-crear-blog',
  templateUrl: './crear-blog.component.html',
  styleUrls: ['./crear-blog.component.scss']
})
export class CrearBlogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<CrearPlanMembresiaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public fb:FormBuilder,
    public blogService:BlogService,
    public _snackBar:MatSnackBar,
    public categoriaService:CategoriaService
    ) {}

    crearBlogForm = this.fb.group({
      titulo: ['',[Validators.required]],
      idCategoria: ['',[Validators.required]]
    })

  onNoClick(): void {
    this.dialogRef.close();
  }

  
categorias
  getCategorias(){
    this.categoriaService.getCategorias().subscribe((resp:any)=>{
      console.log(resp)
      if(resp.ok){
        this.categorias = resp.categorias
      }else{
        this._snackBar.open('Ocurrio un error al intentar obtener las categorias, intentalo denuevo','aceptar')
      }
    })
  }
  

  crearBlog(){

    this.blogService.setBlog(this.crearBlogForm.value).subscribe((resp:any)=>{
      if(resp.ok){
       this.dialogRef.close(true)
      }else{
        let snackBarRef =  this._snackBar.open('El paquete de credito No se pudo guardar, intente denuevo o verifica la informacion','aceptar',{
          duration:3000
        })

      }
    })
  }
  ngOnInit() {
    this.getCategorias()
  }



}
