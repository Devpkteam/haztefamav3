import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { CrearCategoriaComponent } from '../dialogs-admin/crear-categoria/crear-categoria.component';
import { EditarCategoriaComponent } from '../dialogs-admin/editar-categoria/editar-categoria.component';
import { EditarDisputaComponent } from '../dialogs-admin/editar-disputa/editar-disputa.component';

@Component({
  selector: 'app-administrar-disputa',
  templateUrl: './administrar-disputa.component.html',
  styleUrls: ['./administrar-disputa.component.scss']
})
export class AdministrarDisputaComponent implements OnInit {

  displayedColumns: string[] = ['objeto','nombre_servicio','tipo_usuario','fecha', 'editar','ver','estado'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  data = [
    {id:'1',objeto:'un objeto de disputa',nombre_servicio:'un nombre de servicio',tipo_usuario:'Cliente',fecha:'02/02/2020',estado:true},
    {id:'2',objeto:'otro objeto de disputa',nombre_servicio:'otro nombre de servicio',tipo_usuario:'Proveedor',fecha:'03/02/2020',estado: true}
  ]

  constructor(public dialog: MatDialog) {
    // Create 100 users
   

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.data);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editDisputa(data){
    let editDisputaDialog = this.dialog.open(EditarDisputaComponent,{
      width: '400px',
      height: '400px',
      data:data
    })

    
  }


  editCategory(data){
    let editCategoryDialog = this.dialog.open(EditarCategoriaComponent,{
      width: '400px',
      height: '250px',
      data:data
    })
  }

}
