import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material';

import {UsuarioService} from '../../../services/usuarios.service';
import { EditarUsuarioComponent } from '../dialogs-admin/editar-usuario/editar-subcategoria.component';



@Component({
  selector: 'app-administracion-de-usuarios',
  templateUrl: './administracion-de-usuarios.component.html',
  styleUrls: ['./administracion-de-usuarios.component.scss']
})
export class AdministracionDeUsuariosComponent implements OnInit {

  displayedColumns: string[] = ['usuario', 'tipo', 'ver','eliminar', 'estado'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  usuarios:any;

  data = [
    {id:'1',usuario:'Juan',tipo:'Cliente'},
    {id:'2',usuario:'David',tipo:'Proveedor'}
  ]

  constructor(public dialog: MatDialog, public usuarioService: UsuarioService) {
    this.dataSource = new MatTableDataSource(this.usuarios);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.usuarioService.obtenerUsuarios().subscribe((res) => {
      this.usuarios = res;
      console.log(this.usuarios);
      this.dataSource = new MatTableDataSource(this.usuarios);
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  deleteUser(id){
    console.log(id)
    this.usuarioService.borrarUsuario(id).subscribe((res)=>{
      // TODO cambiar por snackbar
      alert('usuario borrado')
      console.log(res)
      this.ngOnInit()
    });
  }

  editUser(user){
    console.log(user)
    let editCategoryDialog = this.dialog.open(EditarUsuarioComponent,{
      width: '400px',
      height: '600px',
      data:user
    })
  }
}
