import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog, MatSnackBar } from '@angular/material';
import { CrearCategoriaComponent } from '../dialogs-admin/crear-categoria/crear-categoria.component';
import { EditarCategoriaComponent } from '../dialogs-admin/editar-categoria/editar-categoria.component';
import { AceptarAlertComponent } from '../dialogs-admin/aceptar-alert/aceptar-alert.component';
import { CategoriaService } from '../../services/categoria.service';


@Component({
  selector: 'app-administrar-categoria',
  templateUrl: './administrar-categoria.component.html',
  styleUrls: ['./administrar-categoria.component.scss']
})
export class AdministrarCategoriaComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'editar','eliminar','estado'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  

  constructor(public dialog: MatDialog, private _snackBar:MatSnackBar, public categoriaService:CategoriaService) {
    // Create 100 users
   

    // Assign the data to the data source for the table to render
   
  }

  ngOnInit() {
    this.getCategorias()
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getCategorias(){
    this.categoriaService.getCategorias().subscribe((resp:any)=>{
      if(resp.ok){
      
        this.dataSource = new MatTableDataSource(resp.categorias);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }else{
        this._snackBar.open('ocurrio un error al intentar obtener las categorias','aceptar')
      }
    })
  }

  newCategory(){
    let newCategoryDialog = this.dialog.open(CrearCategoriaComponent,{
      width: '400px',
      height: '250px'
    })

    newCategoryDialog.afterClosed().subscribe((resp)=>{
      if(resp){
        this.getCategorias();
      }
    })
    
  }


  editCategory(data){
    let editCategoryDialog = this.dialog.open(EditarCategoriaComponent,{
      width: '400px',
      height: '250px',
      data:data
    })

    editCategoryDialog.afterClosed().subscribe((resp)=>{
      if(resp){
        this._snackBar.open('se edito correctamente','aceptar',{duration:3000})
        this.getCategorias()
      }
    })

    
  }


  async eliminarCategoria(id){
    const alertModal = this.dialog.open(AceptarAlertComponent,{
      width: '350px',
      height: '150px',
      data: {msg:'Seguro que deseas borrar?'}
    })

    await alertModal.afterClosed().subscribe((result)=>{
      if(result){

        this._snackBar.open('Borrando paquete!', 'Aceptar!')

        this.categoriaService.deleteCategoria(id).subscribe((resp:any)=>{
          if(resp.ok){
            this._snackBar.open('Categoria eliminada de la base de datos!', 'Aceptar!', {
              duration: 4000,
           
            
          })
           this.getCategorias()
            
          }
        })
        
        
    }
    })
   
    
  }

 

  async cambiarEstado(categoria){
    const alertModal = this.dialog.open(AceptarAlertComponent,{
      width: '350px',
      height: '170px',
      data: {msg:'Seguro que deseas cambiar el estado?'}
    })

    await alertModal.afterClosed().subscribe((result)=>{
      if(result){

        this._snackBar.open('Cambiando el estado de la categoria!', 'Aceptar!')
       this.categoriaService.updateCategoria(categoria._id,{estado: !categoria.estado}).subscribe((resp:any)=>{
         if(resp.ok){
          this._snackBar.open('Se cambio el estado de la categoria!', 'Aceptar!',{duration:3000})
          this.getCategorias()

         }else{
          this._snackBar.open('Ocurrio un error al intentar cambiar el estado!', 'Aceptar!',{duration:3000})

         }
       })
        
        
    }
    })
   
  }
}
