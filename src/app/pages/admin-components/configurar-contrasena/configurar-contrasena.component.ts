import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-configurar-contrasena',
  templateUrl: './configurar-contrasena.component.html',
  styleUrls: ['./configurar-contrasena.component.scss']
})
export class ConfigurarContrasenaComponent implements OnInit {

  constructor(
    public usuarioService:UsuarioService,
    public fb:FormBuilder,
    private _snackBar:MatSnackBar
    ) { }

  ngOnInit() {
  }


editPasswordForm = this.fb.group({
  current_password: ['',[Validators.required,Validators.minLength(8)]],
  new_password: ['',[Validators.required,Validators.minLength(8)]],
  repeat_password: ['',[Validators.required,Validators.minLength(8)]]

},{validator: this.checkPasswords })

checkPasswords(form: FormGroup) { // funcion syncrona para verificar que las contraseñas coinciden
  let pass = form.controls.new_password.value;
  let confirmPass = form.controls.repeat_password.value;
  return pass !== confirmPass ? { 'repeatInvalid' : true } : null     
}

changePassword(){
 
  this.usuarioService.cambiarPassword(localStorage.getItem('id'),this.editPasswordForm.value).subscribe((resp:any)=>{
    if(resp.ok){
      this.editPasswordForm.reset()
      this._snackBar.open('La contraseña se ha actualizado','Aceptar')
    }else if(resp.err == 2){
      this._snackBar.open('La contraseña actual es incorrecta','Aceptar')

    }
  })
}


  hide = true;
  hide2 = true;
  hide3 = true;

}
