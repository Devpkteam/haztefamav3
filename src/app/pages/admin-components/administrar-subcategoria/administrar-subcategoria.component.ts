import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog, MatSnackBar } from '@angular/material';
import { CrearCategoriaComponent } from '../dialogs-admin/crear-categoria/crear-categoria.component';
import { EditarCategoriaComponent } from '../dialogs-admin/editar-categoria/editar-categoria.component';
import { EditarSubcategoriaComponent } from '../dialogs-admin/editar-subcategoria/editar-subcategoria.component';
import { CrearSubcategoriaComponent } from '../dialogs-admin/crear-subcategoria/crear-subcategoria.component';
import { SubcategoriaService } from '../../services/subcategoria.service';
import { AceptarAlertComponent } from '../dialogs-admin/aceptar-alert/aceptar-alert.component';

@Component({
  selector: 'app-administrar-subcategoria',
  templateUrl: './administrar-subcategoria.component.html',
  styleUrls: ['./administrar-subcategoria.component.scss']
})
export class AdministrarSubcategoriaComponent implements OnInit {

  displayedColumns: string[] = ['nombre','categoria', 'editar','eliminar','estado'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  

  constructor(public dialog: MatDialog,
    private _snackBar:MatSnackBar,
    public subcategoriasService:SubcategoriaService) {
    // Create 100 users
   

    // Assign the data to the data source for the table to render
  }

  ngOnInit() {
    this.getSubcategorias();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  newSubCategory(){
    let newSubCategoryDialog = this.dialog.open(CrearSubcategoriaComponent,{
      width: '400px',
      height: '270px'
    })

    newSubCategoryDialog.afterClosed().subscribe((resp)=>{
      if(resp){
        this._snackBar.open('Se creo correctamente','Aceptar',{duration:3000})
        this.getSubcategorias()
      }
    })
  }

  getSubcategorias(){
    this.subcategoriasService.getSubcategorias().subscribe((resp:any)=>{
      if(resp.ok){
        this.dataSource = new MatTableDataSource(resp.subcategorias);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    
      }else{
        this._snackBar.open('Ocurrio un error al intentar buscar las subcategorias','Aceptar')

      }
    })
  }


  editSubCategory(data){
    let editSubCategoryDialog = this.dialog.open(EditarSubcategoriaComponent,{
      width: '400px',
      height: '270px',
      data:data
    })

    editSubCategoryDialog.afterClosed().subscribe((resp)=>{
      if(resp){
        this._snackBar.open('Se edito correctamente','aceptar');
        this.getSubcategorias()
      }
    })
  }

  async eliminarSubcategoria(id){
    const alertModal = this.dialog.open(AceptarAlertComponent,{
      width: '350px',
      height: '150px',
      data: {msg:'Seguro que deseas borrar?'}
    })

    await alertModal.afterClosed().subscribe((result)=>{
      if(result){

        this._snackBar.open('Borrando subcategoria!', 'Aceptar!')

        this.subcategoriasService.deleteSubcategoria(id).subscribe((resp:any)=>{
          if(resp.ok){
            this._snackBar.open('Subcategoria eliminada de la base de datos!', 'Aceptar!', {
              duration: 4000,
           
            
          })
           this.getSubcategorias()
            
          }
        })
        
        
    }
    })
   
    
  }

  async cambiarEstado(subcategoria){
    const alertModal = this.dialog.open(AceptarAlertComponent,{
      width: '350px',
      height: '170px',
      data: {msg:'Seguro que deseas cambiar el estado?'}
    })

    await alertModal.afterClosed().subscribe((result)=>{
      if(result){

        this._snackBar.open('Cambiando el estado de la categoria!', 'Aceptar!')
       this.subcategoriasService.updateSubcategoria(subcategoria._id,{estado: !subcategoria.estado}).subscribe((resp:any)=>{
         if(resp.ok){
          this._snackBar.open('Se cambio el estado de la categoria!', 'Aceptar!',{duration:3000})
          this.getSubcategorias()

         }else{
          this._snackBar.open('Ocurrio un error al intentar cambiar el estado!', 'Aceptar!',{duration:3000})

         }
       })
        
        
    }
    })
   
  }
}
