import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-administrar-comentarios-blogs',
  templateUrl: './administrar-comentarios-blogs.component.html',
  styleUrls: ['./administrar-comentarios-blogs.component.scss']
})
export class AdministrarComentariosBlogsComponent implements OnInit {

 
  displayedColumns: string[] = ['fecha', 'blog','usuario','comentario','eliminar'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  data = [
    {id:'1',fecha:'02/02/2020',blog:'an awesome blog',usuario:'david',comentario:'Me encanto este blog',estado:true},
  ]

  constructor(public dialog: MatDialog) {
    // Create 100 users
   

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.data);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  
}
