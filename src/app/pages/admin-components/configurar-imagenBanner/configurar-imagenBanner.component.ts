import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../../../config/config';

@Component({
  selector: 'app-configurar-imagenBanner',
  templateUrl: './configurar-imagenBanner.component.html',
  styleUrls: ['./configurar-imagenBanner.component.scss']
})
export class ConfigurarImagenBannerComponent implements OnInit {

  constructor(public http:HttpClient) { }

  ngOnInit() {
  }

  image;

  onFileChange(event){
    this.image = event.target.files[0];
    console.log(event);
  }

  changeImage(){
    const formData = new FormData();
  
    formData.append('file', this.image);

    this.http.post<any>(`${URL_SERVICIOS}/imagen-banner`, formData).subscribe((resp=>{
      window.location.reload()
    }))

  }
}
