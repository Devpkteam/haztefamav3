import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-administrar-categorias-blog',
  templateUrl: './administrar-categorias-blog.component.html',
  styleUrls: ['./administrar-categorias-blog.component.scss']
})
export class AdministrarCategoriasBlogComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'editar','eliminar','ver','estado'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  data = [
    {id:'1',nombre:'una categoria de blog',estado:true},
    {id:'2',nombre:'otra categoria de blog',estado: true}
  ]

  constructor(public dialog: MatDialog) {
    // Create 100 users
   

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.data);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
