import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-ver-comentario-dialog',
  templateUrl: './ver-comentario-dialog.component.html',
  styleUrls: ['./ver-comentario-dialog.component.scss']
})
export class VerComentarioDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<VerComentarioDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

}
