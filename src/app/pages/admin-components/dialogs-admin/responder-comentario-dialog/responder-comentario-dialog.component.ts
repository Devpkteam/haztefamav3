import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-responder-comentario-dialog',
  templateUrl: './responder-comentario-dialog.component.html',
  styleUrls: ['./responder-comentario-dialog.component.scss']
})
export class ResponderComentarioDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ResponderComentarioDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

}
