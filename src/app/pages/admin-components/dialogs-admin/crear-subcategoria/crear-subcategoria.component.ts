import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { CategoriaService } from '../../../services/categoria.service';
import { SubcategoriaService } from '../../../services/subcategoria.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-crear-subcategoria',
  templateUrl: './crear-subcategoria.component.html',
  styleUrls: ['./crear-subcategoria.component.scss']
})
export class CrearSubcategoriaComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CrearSubcategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public categoriaService:CategoriaService,
    public subcategoriaService:SubcategoriaService,
    private _snackBar:MatSnackBar,
    public fb:FormBuilder) {}


    categorias

    
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.getCategorias()
  }

  nuevaSubcategoriaForm = this.fb.group({
    nombre: ['',[Validators.required]],
    idCategoria: ['',[Validators.required]]
  })

  crearSubcategoria(){
    this.subcategoriaService.setSubcategoria(this.nuevaSubcategoriaForm.value)
      .subscribe((resp:any)=>{
        if(resp.ok){
          this.dialogRef.close(true)
        }else{
          this._snackBar.open('Ocurrio un error al intentar crear la subcategoria, intentalo nuevamente','Aceptar')
        }
      })
  }

  getCategorias(){
    this.categoriaService.getCategorias().subscribe((resp:any)=>{
      console.log(resp)
      if(resp.ok){
        this.categorias = resp.categorias
      }else{
        this._snackBar.open('Ocurrio un error al intentar obtener las categorias, intentalo denuevo','aceptar')
      }
    })
  }
}
