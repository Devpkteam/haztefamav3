import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBarModule, MatSnackBar } from '@angular/material';
import { Validators, FormBuilder } from '@angular/forms';
import { PaquetesDeCreditoService } from '../../../services/paquetes-de-credito.service';
@Component({
  selector: 'app-editar-plan-membresia',
  templateUrl: './editar-plan-membresia.component.html',
  styleUrls: ['./editar-plan-membresia.component.scss']
})
export class EditarPlanMembresiaComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EditarPlanMembresiaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public fb:FormBuilder,
    public paquetesService:PaquetesDeCreditoService,
    private _snackBar:MatSnackBar) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

  editarPaqueteDeCreditoForm = this.fb.group({
    nombre: [this.data.nombre,[Validators.required]],
    precio: [this.data.precio,[Validators.required]],
    cantidad_de_creditos: [this.data.cantidad_de_creditos,[Validators.required]],
    metodo_de_pago: [this.data.metodo_de_pago,[Validators.required]]

  })

  editarPaquete(){
    this.paquetesService.updatePaquete(this.data._id,this.editarPaqueteDeCreditoForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        let snackBarRef = this._snackBar.open(`El paquete ${resp.paqueteUpdate.nombre} se ha modificado correctamente`,'aceptar',{
          duration:3000
        })
        snackBarRef.afterDismissed().subscribe(() => {
          this.dialogRef.close(true);
        });

        snackBarRef.onAction().subscribe(() => {
          this.dialogRef.close(true);
        });

      }
    })
  }

}
