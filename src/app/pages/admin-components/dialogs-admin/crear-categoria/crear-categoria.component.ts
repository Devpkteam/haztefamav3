import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { CategoriaService } from '../../../services/categoria.service';

@Component({
  selector: 'app-crear-categoria',
  templateUrl: './crear-categoria.component.html',
  styleUrls: ['./crear-categoria.component.scss']
})
export class CrearCategoriaComponent implements OnInit {


  constructor(
    public dialogRef: MatDialogRef<CrearCategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public fb:FormBuilder,
    public categoriaService:CategoriaService,
    private _snackBar:MatSnackBar
    ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

  nuevaCategoriaForm = this.fb.group({
    nombre: ['',[Validators.required]]
  })

cerrarDialog(status){
  this.dialogRef.close(status);
}

  crearCategoria(){
    this._snackBar.open('creando una nueva categoria')
      this.categoriaService.setCategoria(this.nuevaCategoriaForm.value).subscribe((resp:any)=>{
        if(resp.ok){
          this.nuevaCategoriaForm.reset()
          let snackBarRef = this._snackBar.open('Se ha creado correctamente!','aceptar',{duration:3000})
          
          snackBarRef.afterDismissed().subscribe(() => {
            this.cerrarDialog(true)
          });
          snackBarRef.onAction().subscribe(() => {
            this.cerrarDialog(true)
          });
          

        }else{

        }
      })
  }

}
