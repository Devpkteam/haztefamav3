import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-editar-disputa',
  templateUrl: './editar-disputa.component.html',
  styleUrls: ['./editar-disputa.component.scss']
})
export class EditarDisputaComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EditarDisputaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

}
