import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { CategoriaService } from '../../../services/categoria.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-editar-categoria',
  templateUrl: './editar-categoria.component.html',
  styleUrls: ['./editar-categoria.component.scss']
})
export class EditarCategoriaComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EditarCategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _snackBar:MatSnackBar,
    public categoriaService:CategoriaService,
    public fb:FormBuilder) {}



  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

  editarCategoriaForm = this.fb.group({
    nombre: [this.data.nombre,[Validators.required]]
  })

  editarCategoria(){
    this.categoriaService.updateCategoria(this.data._id,this.editarCategoriaForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.dialogRef.close(true)
      }else{
        this._snackBar.open('ocurrio un error, intentalo denuevo','aceptar')
      }
    })
  }

}
