import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-servicio-requerido',
  templateUrl: './servicio-requerido.component.html',
  styleUrls: ['./servicio-requerido.component.scss']
})
export class ServicioRequeridoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ServicioRequeridoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }
}
