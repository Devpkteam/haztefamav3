import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { PaquetesDeCreditoService } from '../../../services/paquetes-de-credito.service';

@Component({
  selector: 'app-crear-plan-membresia',
  templateUrl: './crear-plan-membresia.component.html',
  styleUrls: ['./crear-plan-membresia.component.scss']
})
export class CrearPlanMembresiaComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CrearPlanMembresiaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public fb:FormBuilder,
    public paquetesService:PaquetesDeCreditoService,
    public _snackBar:MatSnackBar
    ) {}

    crearPaqueteDeCreditoForm = this.fb.group({
      nombre: ['',[Validators.required]],
      precio: ['',[Validators.required]],
      cantidad_de_creditos: ['',[Validators.required]],
      metodo_de_pago: ['',[Validators.required]]

    })

  onNoClick(): void {
    this.dialogRef.close();
  }

  crearPaquete(){

    this.paquetesService.setPaquete(this.crearPaqueteDeCreditoForm.value).subscribe((resp:any)=>{
      if(resp.ok){
        this.crearPaqueteDeCreditoForm.reset();
        let snackBarRef =  this._snackBar.open('El paquete de credito se guardo en la base de datos','Aceptar',{
          duration:4000
        })

        snackBarRef.afterDismissed().subscribe(() => {
          this.onNoClick();
        });

        snackBarRef.onAction().subscribe(() => {
          this.onNoClick();
        });
      }else{
        let snackBarRef =  this._snackBar.open('El paquete de credito No se pudo guardar, intente denuevo o verifica la informacion','aceptar',{
          duration:3000
        })

      }
    })
  }
  ngOnInit() {
  }


}
