import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface AceptarData {
  msg:string
}

@Component({
  selector: 'app-aceptar-alert',
  templateUrl: './aceptar-alert.component.html',
  styleUrls: ['./aceptar-alert.component.scss']
})
export class AceptarAlertComponent implements OnInit {

  ngOnInit() {
  }
  constructor(
    public alertModal: MatDialogRef<AceptarAlertComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AceptarData
  ) { }

    Aceptar(){
      this.alertModal.close(true)
    }

    Cancelar(){
      this.alertModal.close(false)
    }
    

}
