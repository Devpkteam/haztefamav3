import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { VerComentarioDialogComponent } from '../dialogs-admin/ver-comentario-dialog/ver-comentario-dialog.component';
import { MatDialog } from '@angular/material';
import { ResponderComentarioDialogComponent } from '../dialogs-admin/responder-comentario-dialog/responder-comentario-dialog.component';







@Component({
  selector: 'app-configurar-gestion-comentarios',
  templateUrl: './configurar-gestion-comentarios.component.html',
  styleUrls: ['./configurar-gestion-comentarios.component.scss']
})
export class ConfigurarGestionComentariosComponent implements OnInit {

  displayedColumns: string[] = ['fecha', 'usuario', 'ver','responder'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  data = [
    {id:'1',usuario:'Juan',comentario:'Los servicios son excelentes',fecha:'03/02/2020'},
    {id:'2',usuario:'David',comentario:'Me encanto, lo recomiendo',fecha:'04/02/2020'}
  ]

  constructor(public dialog: MatDialog) {
    // Create 100 users
   

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.data);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  
replyCommentary(data){
  const cometarioDialog = this.dialog.open(ResponderComentarioDialogComponent, {
    width: '500px',
    height: '300px',
    data: data
  });

  cometarioDialog.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    
  });
}

seeCommentary(data){
  console.log(data,'ver')
  const dialogRef = this.dialog.open(VerComentarioDialogComponent, {
    width: '500px',
    height: '300px',
    data: data
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    
  });
}



}

/** Builds and returns a new User. */

