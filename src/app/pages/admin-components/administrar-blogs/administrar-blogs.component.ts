import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog, MatSnackBar } from '@angular/material';
import { CrearBlogComponent } from '../blogs-components/crear-blog/crear-blog.component';
import { EditarBlogComponent } from '../blogs-components/editar-blog/editar-blog.component';
import { AceptarAlertComponent } from '../dialogs-admin/aceptar-alert/aceptar-alert.component';
import { BlogService } from '../../services/blog.service';

@Component({
  selector: 'app-administrar-blogs',
  templateUrl: './administrar-blogs.component.html',
  styleUrls: ['./administrar-blogs.component.scss']
})
export class AdministrarBlogsComponent implements OnInit {

  displayedColumns: string[] = ['titulo', 'categoria','editar','eliminar','estado'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  data = [
    {id:'1',titulo:'La gastronomia del futuro',categoria:'gastronomia',estado:true},
  ]

  constructor(public dialog: MatDialog,private _snackBar:MatSnackBar,public blogService:BlogService) {
    // Create 100 users
   

    // Assign the data to the data source for the table to render

  }

  ngOnInit() {
    this.getBlogs()
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getBlogs(){
    this.blogService.getBlogs().subscribe((resp:any)=>{
      if(resp.ok){
        this.dataSource = new MatTableDataSource(resp.blogs);
        this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
      }
    })
  }

  newBlog(){
    const newBlogDialog = this.dialog.open(CrearBlogComponent, {
      width: '500px',
      height: '300px'
    });
  
    newBlogDialog.afterClosed().subscribe(result => {
      if(result){
        this._snackBar.open('El blog se creo correctamente','aceptar');
        this.getBlogs()
      }
      
    });
  }

  editBlog(data){
    const editPlanDialog = this.dialog.open(EditarBlogComponent, {
      width: '500px',
      height: '300px',
      data: data
    });
  
    editPlanDialog.afterClosed().subscribe(result => {
      if(result){
        this._snackBar.open('El blog modifico correctamente!', 'Aceptar!')
        this.getBlogs()
      }
      
    });
  }

  async eliminarBlog(id){
    const alertModal = this.dialog.open(AceptarAlertComponent,{
      width: '350px',
      height: '150px',
      data: {msg:'Seguro que deseas borrar?'}
    })

    await alertModal.afterClosed().subscribe((result)=>{
      if(result){

        this._snackBar.open('Borrando Blog!', 'Aceptar!')

       this.blogService.deleteBlog(id).subscribe((resp:any)=>{
         if(resp.ok){
          this._snackBar.open('El blog se elimino de la base de datos!', 'Aceptar!')
          this.getBlogs()

         }else{
          this._snackBar.open('Ocurrio un error al intentar eliminar el  Blog!', 'Aceptar!')

         }

       })
        
        
    }
    })
   
    
  }

  async cambiarEstado(blog){
    const alertModal = this.dialog.open(AceptarAlertComponent,{
      width: '350px',
      height: '170px',
      data: {msg:'Seguro que deseas cambiar el estado?'}
    })

    await alertModal.afterClosed().subscribe((result)=>{
      if(result){

        this._snackBar.open('Cambiando el estado de la categoria!', 'Aceptar!')
       this.blogService.updateBlog(blog._id,{estado: !blog.estado}).subscribe((resp:any)=>{
         if(resp.ok){
          this._snackBar.open('Se cambio el estado de la categoria!', 'Aceptar!',{duration:3000})
          this.getBlogs()

         }else{
          this._snackBar.open('Ocurrio un error al intentar cambiar el estado!', 'Aceptar!',{duration:3000})

         }
       })
        
        
    }
    })
   
  }

}
