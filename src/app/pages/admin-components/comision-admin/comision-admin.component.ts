import { Component, OnInit } from '@angular/core';
import { CategoriaService } from '../../services/categoria.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { SubcategoriaService } from '../../services/subcategoria.service';

@Component({
  selector: 'app-comision-admin',
  templateUrl: './comision-admin.component.html',
  styleUrls: ['./comision-admin.component.scss']
})
export class ComisionAdminComponent implements OnInit {

  constructor(public categoriaService:CategoriaService,
    public subcategoriaService:SubcategoriaService,
    private _snackBar:MatSnackBar,
    public fb:FormBuilder) { }

  ngOnInit() {
    this.getSubcategorias()
    this.getCategorias()
  }

  optionC = 'ca';
  idCategoria;
  idSubcategoria;

  categoriaComisionForm = this.fb.group({
    _id: ['',[Validators.required]],
    comision: ['',[Validators.required]]
  
  })

  subcategoriaComisionForm = this.fb.group({
    _id: ['',[Validators.required]],
    comision: ['',[Validators.required]]
  })

  categorias
  getCategorias(){
    this.categoriaService.getCategorias().subscribe((resp:any)=>{
     
      if(resp.ok){
        this.categorias = resp.categorias
      }else{
        this._snackBar.open('Ocurrio un error al intentar obtener las categorias, intentalo denuevo','aceptar')
      }
    })
  }

  subcategorias
  getSubcategorias(){
    this.subcategoriaService.getSubcategorias().subscribe((resp:any)=>{
      
      if(resp.ok){
        this.subcategorias = resp.subcategorias
      }else{
        this._snackBar.open('Ocurrio un error al intentar obtener las categorias, intentalo denuevo','aceptar')
      }
    })
  }

  getCategoria(id){
  this.categoriaService.getCategoria(id).subscribe((resp:any)=>{
    if(resp.ok){
      this.categoriaComisionForm.controls.comision.setValue(resp.categoria.comision);
      this.getCategorias()
    }else{
      this._snackBar.open('Ocurrio un error inesperado, intentalo denuevo','aceptar')

    }
  })
  }

  getSubcategoria(id){
    this.subcategoriaService.getSubcategoria(id).subscribe((resp:any)=>{
      if(resp.ok){
        
        this.subcategoriaComisionForm.controls.comision.setValue(resp.subcategoria.comision);
        this.getSubcategorias()
      }else{
        this._snackBar.open('Ocurrio un error inesperado, intentalo denuevo','aceptar')
  
      }
    })
    }

  editarCategoria(){
    this.categoriaService.updateCategoria(this.categoriaComisionForm.value._id,{comision:this.categoriaComisionForm.value.comision})
      .subscribe((resp:any)=>{
        if(resp.ok){
          this._snackBar.open('La comision se establecio correctamente','aceptar')
          this.getCategorias()
        }else{
          this._snackBar.open('Hubo un error al intentar cambiar el monto de la comision','aceptar')

        }
      })
  }

  editarSubcategoria(){
    this.subcategoriaService.updateSubcategoria(this.subcategoriaComisionForm.value._id,{comision:this.subcategoriaComisionForm.value.comision})
      .subscribe((resp:any)=>{
        if(resp.ok){
          this._snackBar.open('La comision se establecio correctamente','aceptar')
          this.getSubcategorias()
        }else{
          this._snackBar.open('Hubo un error al intentar cambiar el monto de la comision','aceptar')

        }
      })
  }

}
