import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material';


@Component({
  selector: 'app-pagos-servicios',
  templateUrl: './pagos-servicios.component.html',
  styleUrls: ['./pagos-servicios.component.scss']
})
export class PagosServiciosComponent implements OnInit {
  displayedColumns: string[] = ['fecha', 'transaccion','nombre_cliente','nombre_proveedor','nombre_servicio','costo_total','comision_admin','estado_pago','estado_servicio'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  data = [
    {id:'1',nombre_servicio:'un plan',transaccion:'12300429942',fecha:'03/02/2020 02:33:00',monto:'30',comision_admin:'3',nombre_cliente:'samuel',nombre_proveedor:'david',estado_servicio:true,estado_pago:true},
  ]

  constructor(public dialog: MatDialog) {
    // Create 100 users
   

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.data);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
