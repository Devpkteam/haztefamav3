import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog, MatSnackBar } from '@angular/material';
import { CrearPlanMembresiaComponent } from '../dialogs-admin/crear-plan-membresia/crear-plan-membresia.component';
import { EditarPlanMembresiaComponent } from '../dialogs-admin/editar-plan-membresia/editar-plan-membresia.component';
import { PaquetesDeCreditoService } from '../../services/paquetes-de-credito.service';
import { AceptarAlertComponent } from '../dialogs-admin/aceptar-alert/aceptar-alert.component';


@Component({
  selector: 'app-paquetes-de-credito',
  templateUrl: './paquetes-de-credito.component.html',
  styleUrls: ['./paquetes-de-credito.component.scss']
})
export class PaquetesDeCreditoComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'precio', 'creditos','paymentMethod','editar','estado','eliminar'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  data;

  constructor(public dialog: MatDialog, 
              public paquetesService:PaquetesDeCreditoService,
              private _snackBar: MatSnackBar,
              public PaquetesService:PaquetesDeCreditoService) {
    // Create 100 users
   

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.data);
  }

  ngOnInit() {
    this.getPaquetes()
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getPaquetes(){
    this.paquetesService.getPaquetes().subscribe((resp:any)=>{
        if(resp.ok){
          this.dataSource =  new MatTableDataSource(resp.paquetesDeCredito);
          this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
        }
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  newPlan(){
    const newPlanDialog = this.dialog.open(CrearPlanMembresiaComponent, {
      width: '500px',
      height: '430px'
    });
  
    newPlanDialog.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }

 editPlan(data){
    const editPlanDialog = this.dialog.open(EditarPlanMembresiaComponent, {
      width: '500px',
      height: '350px',
      data: data
    });
  
    editPlanDialog.afterClosed().subscribe(result => {
      if(result){
        this.getPaquetes()
      }
      
    });
  }

 async cambiarEstado(plan){
    const alertModal = this.dialog.open(AceptarAlertComponent,{
      width: '350px',
      height: '170px',
      data: {msg:'Seguro que deseas cambiar el estado?'}
    })

    await alertModal.afterClosed().subscribe((result)=>{
      if(result){

        this._snackBar.open('Borrando paquete!', 'Aceptar!')
        console.log(plan.estado)
        this.paquetesService.updatePaquete(plan._id,{estado: !plan.estado}).subscribe((resp:any)=>{
          if(resp.ok){
            console.log(resp)
            this._snackBar.open(`Paquete estado cambiado a ${resp.paqueteUpdate.estado}!`, 'Aceptar!', {
              duration: 4000,
           
            
          })
           this.getPaquetes()
            
          }
        })
        
        
    }
    })
   
  }

  async eliminarPlan(id){
    const alertModal = this.dialog.open(AceptarAlertComponent,{
      width: '350px',
      height: '150px',
      data: {msg:'Seguro que deseas borrar?'}
    })

    await alertModal.afterClosed().subscribe((result)=>{
      if(result){

        this._snackBar.open('Borrando paquete!', 'Aceptar!')

        this.paquetesService.deletePaquete(id).subscribe((resp:any)=>{
          if(resp.ok){
            this._snackBar.open('Paquete eliminado de la base de datos!', 'Aceptar!', {
              duration: 4000,
           
            
          })
           this.getPaquetes()
            
          }
        })
        
        
    }
    })
   
    
  }
  

}
