import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { ServicioRequeridoComponent } from '../dialogs-admin/servicio-requerido/servicio-requerido.component';

@Component({
  selector: 'app-servicios-requeridos',
  templateUrl: './servicios-requeridos.component.html',
  styleUrls: ['./servicios-requeridos.component.scss']
})
export class ServiciosRequeridosComponent implements OnInit {
  displayedColumns: string[] = ['servicio', 'cliente', 'ver','estado'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  data = [
    {id:'1',servicio:'servicio de gastronomia',cliente:'David',estado:true,desc:'Quiero un servicio de gastronomia, por favor'},
    {id:'2',servicio:'servicio de pagos por paypal',cliente:'Juan',estado: true,desc:'Quier un servicio con pagos por paypal'}
  ]

  constructor(public dialog: MatDialog) {
    // Create 100 users
   

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.data);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  seeServiceRequired(data){
    let serviceRequiredDialog = this.dialog.open(ServicioRequeridoComponent,{
      width:'400px',
      height:'300px',
      data:data
    })
  }
}
