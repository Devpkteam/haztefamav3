import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { PlantillaCorreoService } from '../../services/plantilla-correo.service';

@Component({
  selector: 'app-configurar-plantilla-correo',
  templateUrl: './configurar-plantilla-correo.component.html',
  styleUrls: ['./configurar-plantilla-correo.component.scss']
})
export class ConfigurarPlantillaCorreoComponent implements OnInit {

  constructor(public sanitizer:DomSanitizer, public plantillaCorreoService:PlantillaCorreoService) { }

  ngOnInit() {
    this.getPlantilla()
  }

idPlantilla;
htmlM =  `
<h1>
  Correo electronico de prueba
</h1>

<br>


<h3 style="color: blue;margin-top:30px">
 INSERTAR UN FORMATO 
</h3>





<p style='margin-top:30px'>
por favor no responda a este correo
</p>
`

  html = this.sanitizer.bypassSecurityTrustHtml(this.htmlM);
 ;
  htmltemp;

  textareaHide = true

  edit(){
    this.textareaHide = false
    this.htmltemp = this.html
   
  }

  save(){
    this.plantillaCorreoService.updatePlantilla(this.idPlantilla,{plantilla:this.htmlM}).subscribe((resp:any)=>{
      console.log(resp)
      if(resp.ok){
        this.getPlantilla();
      }
    })
    this.textareaHide = true
  }

  cancel(){
    this.getPlantilla()
    this.textareaHide = true
  }


  getPlantilla(){
    this.plantillaCorreoService.getPlantilla().subscribe((resp:any)=>{
      console.log(resp)
      if(resp.ok){
        if(resp.plantilla == null){
          this.plantillaCorreoService.setPlantilla({plantilla:this.htmlM}).subscribe((resp2:any)=>{
            if(resp2.ok){
              this.getPlantilla()
            }
          })
        }else{
          this.idPlantilla = resp.plantilla._id
          this.htmlM = resp.plantilla.plantilla
          this.html = this.sanitizer.bypassSecurityTrustHtml(resp.plantilla.plantilla)
        }
      }else{

      }
    })
  }


}
