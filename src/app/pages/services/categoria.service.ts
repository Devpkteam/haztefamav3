import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from '../../config/config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  constructor(public http:HttpClient) { }

  setCategoria(body){
    return this.http.post(`${URL_SERVICIOS}/categoria`,body)
  }
  
  getCategoria(id){
    return this.http.get(`${URL_SERVICIOS}/categoria/${id}`)
  
  }
  
  getCategorias(){
    return this.http.get(`${URL_SERVICIOS}/categoria`)
  
  }
  
  deleteCategoria(id){
    return this.http.delete(`${URL_SERVICIOS}/categoria/${id}`)
  
  }
  
  updateCategoria(id,body){
    return this.http.put(`${URL_SERVICIOS}/categoria/${id}`,body)
  
  }

}
