/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PaquetesDeCreditoService } from './paquetes-de-credito.service';

describe('Service: PaquetesDeCredito', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PaquetesDeCreditoService]
    });
  });

  it('should ...', inject([PaquetesDeCreditoService], (service: PaquetesDeCreditoService) => {
    expect(service).toBeTruthy();
  }));
});
