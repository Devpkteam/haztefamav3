import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

constructor(public http:HttpClient) { }

setBlog(body){
  return this.http.post(`${URL_SERVICIOS}/blog`,body)
}

getBlog(id){
  return this.http.get(`${URL_SERVICIOS}/blog/${id}`)

}

getBlogs(){
  return this.http.get(`${URL_SERVICIOS}/blog`)

}

deleteBlog(id){
  return this.http.delete(`${URL_SERVICIOS}/blog/${id}`)

}

updateBlog(id,body){
  return this.http.put(`${URL_SERVICIOS}/blog/${id}`,body)

}





}
