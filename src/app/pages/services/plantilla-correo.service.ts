import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class PlantillaCorreoService {

constructor(public http:HttpClient) { }

setPlantilla(body){
  return this.http.post(`${URL_SERVICIOS}/plantilla-correo`,body)
}

getPlantilla(){
  return this.http.get(`${URL_SERVICIOS}/plantilla-correo`)

}
updatePlantilla(id,body){
  return this.http.put(`${URL_SERVICIOS}/plantilla-correo/${id}`,body)

}

}
