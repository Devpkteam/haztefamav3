import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class PaquetesDeCreditoService {

constructor(public http:HttpClient) { }

  
  setPaquete(body){
    return this.http.post(`${URL_SERVICIOS}/paquetes-credito`,body)
  }

  getPaquetes(){
    return this.http.get(`${URL_SERVICIOS}/paquetes-credito`)

  }

  updatePaquete(id,body){
    return this.http.put(`${URL_SERVICIOS}/paquetes-credito/${id}`,body)

  }

  deletePaquete(id){
    
    return this.http.delete(`${URL_SERVICIOS}/paquetes-credito/${id}`)

  }

  

}
