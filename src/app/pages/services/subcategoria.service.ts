import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from '../../config/config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubcategoriaService {
  
  constructor(public http:HttpClient) { }

  setSubcategoria(body){
    return this.http.post(`${URL_SERVICIOS}/subcategoria`,body)
  }
  
  getSubcategoria(id){
    return this.http.get(`${URL_SERVICIOS}/subcategoria/${id}`)
  
  }
  
  getSubcategorias(){
    return this.http.get(`${URL_SERVICIOS}/subcategoria`)
  
  }
  
  deleteSubcategoria(id){
    return this.http.delete(`${URL_SERVICIOS}/subcategoria/${id}`)
  
  }
  
  updateSubcategoria(id,body){
    return this.http.put(`${URL_SERVICIOS}/subcategoria/${id}`,body)
  
  }


}
