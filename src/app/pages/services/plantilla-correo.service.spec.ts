/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PlantillaCorreoService } from './plantilla-correo.service';

describe('Service: PlantillaCorreo', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlantillaCorreoService]
    });
  });

  it('should ...', inject([PlantillaCorreoService], (service: PlantillaCorreoService) => {
    expect(service).toBeTruthy();
  }));
});
