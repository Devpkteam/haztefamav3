import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

constructor(public http:HttpClient) { }


cambiarPassword(id,body){
 return this.http.put(`${URL_SERVICIOS}/user/password/${id}`,body)
}
}
