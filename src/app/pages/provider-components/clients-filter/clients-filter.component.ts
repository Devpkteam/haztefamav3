import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-clients-filter',
  templateUrl: './clients-filter.component.html',
  styleUrls: ['./clients-filter.component.scss']
})
export class ClientsFilterComponent implements OnInit {

  clients = [];

  constructor( public router:Router ) {  this.Datos() }

  ngOnInit() {
  }

  Datos(){

    this.clients[0] = { "id_client" : 1 , "name_client": "Name Client 1" }
    this.clients[1] = { "id_client" : 2 , "name_client": "Name Client 2" }
    this.clients[2] = { "id_client" : 3 , "name_client": "Name Client 3" }
    this.clients[3] = { "id_client" : 4 , "name_client": "Name Client 4" }
    this.clients[4] = { "id_client" : 5 , "name_client": "Name Client 5" }
    this.clients[5] = { "id_client" : 6 , "name_client": "Name Client 6" }
    this.clients[6] = { "id_client" : 7 , "name_client": "Name Client 7" }

    console.log( this.clients )
  }

  public IrProfileClient(){
    this.router.navigate(['/login/provider/clients-filter/profile'])
  }

}
