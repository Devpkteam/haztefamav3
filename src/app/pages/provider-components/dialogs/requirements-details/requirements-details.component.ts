import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';

@Component({
  selector: 'app-requirements-details',
  templateUrl: './requirements-details.component.html',
  styleUrls: ['./requirements-details.component.scss']
})
export class RequirementsDetailsComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) data,
    public dialogRef: MatDialogRef<RequirementsDetailsComponent>,
  ) { }

  ngOnInit() {
  }

}
