import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequirementsDetailsComponent } from './requirements-details.component';

describe('RequirementsDetailsComponent', () => {
  let component: RequirementsDetailsComponent;
  let fixture: ComponentFixture<RequirementsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequirementsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
