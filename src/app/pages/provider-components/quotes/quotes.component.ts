import { Component, OnInit } from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator, MatDialog} from '@angular/material';

export interface PeriodicElement {
  position: number;
  name: string;
  status:string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', status:'activo'},
  {position: 2, name: 'Helium', status:'activo'},
  {position: 3, name: 'Lithium', status:'activo'},
  {position: 4, name: 'Beryllium', status:'activo'},
  {position: 5, name: 'Boron', status:'activo'},
  {position: 6, name: 'Carbon', status:'activo'},
  {position: 7, name: 'Nitrogen', status:'activo'},
  {position: 8, name: 'Oxygen', status:'activo'},
  {position: 9, name: 'Fluorine', status:'activo'},
  {position: 10, name: 'Neon', status:'activo'},
];

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.scss']
})
export class QuotesComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'status', 'options'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
