import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvClientsProfileComponent } from './prov-clients-profile.component';

describe('ProvClientsProfileComponent', () => {
  let component: ProvClientsProfileComponent;
  let fixture: ComponentFixture<ProvClientsProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvClientsProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvClientsProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
