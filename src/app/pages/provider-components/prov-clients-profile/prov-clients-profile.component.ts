import { Component, OnInit } from '@angular/core';
import { MapaService } from '../../services/mapa.service';

@Component({
  selector: 'app-prov-clients-profile',
  templateUrl: './prov-clients-profile.component.html',
  styleUrls: ['./prov-clients-profile.component.scss']
})
export class ProvClientsProfileComponent implements OnInit {

  constructor( public mapa : MapaService ) { }

  ngOnInit() {

    this.mapa.showMapa()
  }

  b(){
    history.back()
  }


}
