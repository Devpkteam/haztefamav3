import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { ModalSubCategoriesComponent } from '../../../dialogs/modal-sub-categories/modal-sub-categories.component';
import { SubcategoriesService } from '../../../services/subcategories.service';

@Component({
  selector: 'app-home-provider',
  templateUrl: './home-provider.component.html',
  styleUrls: ['./home-provider.component.scss']
})
export class HomeProviderComponent implements OnInit {

  categories = []

  constructor( public dialog: MatDialog, public subServicie:SubcategoriesService ) { console.log("Eres proveedor")  }

  ngOnInit() {
  }

  //Carga los sub menu
public subCategories( e ){

  let id_category = e._elementRef.nativeElement.id ;

  let category = e._elementRef.nativeElement.name ;

console.log("Evento proveedr **********" + category );
console.log( e._elementRef.nativeElement )
console.log(" **********"  );

console.log( e );


 this.subServicie.get_subcategories( category ).length=0;

 let subcategories = this.subServicie.get_subcategories( category )

  //Envia datos al Dialog Modal
 this.showPopotSubcategories( category , category , subcategories);

}


async showPopotSubcategories( titulo , name_category , subcategories ){ //msj: array

  const alertModal = this.dialog.open(ModalSubCategoriesComponent,{
    width: '400px',
    height: '250px',
    data: { perfil:"provider" , titulo: titulo , category : name_category , subcategory : subcategories }
  })

  await alertModal.afterClosed().subscribe((result)=>{

    if(result){
       console.log( "result despues de cerrar" );
       console.log( result )
    }
  })
}//



}
