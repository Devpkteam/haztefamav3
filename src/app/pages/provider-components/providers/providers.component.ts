import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.scss']
})
export class ProvidersComponent implements OnInit {

  providers = [];

  constructor( public router:Router ) {  this.Datos() }

  ngOnInit() {

  }

  Datos(){

    this.providers[0] = { "id_provider" : 1 , "name_provider": "Name Proveedor 1" }
    this.providers[1] = { "id_provider" : 2 , "name_provider": "Name Proveedor 2" }
    this.providers[2] = { "id_provider" : 3 , "name_provider": "Name Proveedor 3" }
    this.providers[3] = { "id_provider" : 4 , "name_provider": "Name Proveedor 4" }
    this.providers[4] = { "id_provider" : 5 , "name_provider": "Name Proveedor 5" }
    this.providers[5] = { "id_provider" : 6 , "name_provider": "Name Proveedor 6" }
    this.providers[6] = { "id_provider" : 7 , "name_provider": "Name Proveedor 7" }

    console.log( this.providers )

    //return providers;
  }

  public IrProfile(){
    this.router.navigate(['/login/client/providers/profile/'])
  }

}
