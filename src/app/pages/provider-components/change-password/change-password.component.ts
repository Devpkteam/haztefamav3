import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  changeForm: FormGroup;

  constructor(
    public form:FormBuilder, 
  ) { 
    this.changeForm = form.group({
      actualPassword: ['', Validators.required],
      password: ['', Validators.required],
      repassword: ['', Validators.required],
      remember: [false]
    });

  }

  ngOnInit() {
  }

  changePassword(){
    
  }

}
