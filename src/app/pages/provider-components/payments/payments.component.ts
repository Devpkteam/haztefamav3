import { Component, OnInit } from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator, MatDialog} from '@angular/material';

export interface PeriodicElement {
  position: number;
  name: string;
  place: string;
  event: string;
  time: string;
  price: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', place: 'aqui',  event: 'aqui', time:'12:04', price:'2.500'},
  {position: 2, name: 'Helium', place: 'aqui',  event: 'aqui', time:'12:04', price:'2.500'},
  {position: 3, name: 'Lithium', place: 'aqui',  event: 'aqui', time:'12:04', price:'2.500'},
  {position: 4, name: 'Beryllium', place: 'aqui',  event: 'aqui', time:'12:04', price:'2.500'},
  {position: 5, name: 'Boron', place: 'aqui',  event: 'aqui', time:'12:04', price:'2.500'},
  {position: 6, name: 'Carbon', place: 'aqui',  event: 'aqui', time:'12:04', price:'2.500'},
  {position: 7, name: 'Nitrogen', place: 'aqui',  event: 'aqui', time:'12:04', price:'2.500'},
  {position: 8, name: 'Oxygen', place: 'aqui',  event: 'aqui', time:'12:04', price:'2.500'},
  {position: 9, name: 'Fluorine', place: 'aqui',  event: 'aqui', time:'12:04', price:'2.500'},
  {position: 10, name: 'Neon', place: 'aqui',  event: 'aqui', time:'12:04', price:'2.500'},
];

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  displayedColumns: string[] = ['position','name','place','event','time','price'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
