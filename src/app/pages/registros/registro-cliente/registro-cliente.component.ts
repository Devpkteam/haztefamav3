import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { RegisterService } from '../../../services/register.service'


@Component({
  selector: 'app-registro-cliente',
  templateUrl: './registro-cliente.component.html',
  styleUrls: ['./registro-cliente.component.css']
})
export class RegistroClienteComponent implements OnInit {

  FormRegister;
  titulo="Mi titulo";
  logo = "/src/assets/img/angular2-logo-white.png";

 //   constructor(public fb:FormBuilder , public alertCrl : AlertController, public corek :CorekProvider, public router: Router ) {

 constructor( public fb:FormBuilder, public registerService: RegisterService , public router:Router ) {

  //Validacion de campos
  this.FormRegister = this.fb.group( {

    name: ['', [Validators.required,  Validators.maxLength(40) ] ],
    email: ['', [Validators.required, Validators.email, Validators.maxLength(50) ] ],
    password: [ '',  [ Validators.required, Validators.minLength(8) ] ],
    phone: [ '',  [Validators.required, Validators.minLength(8),  Validators.maxLength(20) ] ],
    address: [ '', Validators.required ]

 })

  }

  ngOnInit() {
  }

  public register (type){
    console.log("type de formulario");
    console.log(type);
    let info = this.FormRegister.value;

    let body = {
      usuario: info.name,
      nombre: info.name,
      email: info.email,
      password: info.password,
      telefono: info.phone,
      direccion: info.address,
      role: type
    }
    console.log(body)

    this.registerService.register(body).subscribe((res:any)=>{
      console.log(res)
      if(res.ok == true){
        this.router.navigate(['/log'])
      }else{
        alert('El usuario ya existe')
      }
    })
   }


}
