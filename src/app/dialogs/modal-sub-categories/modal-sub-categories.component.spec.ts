import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSubCategoriesComponent } from './modal-sub-categories.component';

describe('ModalSubCategoriesComponent', () => {
  let component: ModalSubCategoriesComponent;
  let fixture: ComponentFixture<ModalSubCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSubCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSubCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
