import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';

export interface DialogData {
  perfil:string,
  titulo:string,
  category:string,
  subcategory:[]
}


@Component({
  selector: 'app-modal-sub-categories',
  templateUrl: './modal-sub-categories.component.html',
  styleUrls: ['./modal-sub-categories.component.scss']
})

export class ModalSubCategoriesComponent implements OnInit {


  constructor( public router:Router,
    public alertModal: MatDialogRef<ModalSubCategoriesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData)  { } //DialogData
  //  constructor(){}
  ngOnInit() {
  }

  onNoClick(){
    this.alertModal.close();
  }

showProvidersXsubcategories( e ){
  let ruta ;
  console.log("categoria: " + this.data.category )
  console.log( "elemento" )
  console.log( e )
  this.onNoClick()

  if( this.data.perfil == "client" ){
    ruta = '/login/client/providers'
    this.router.navigate([ruta])
  }else   if( this.data.perfil == "provider" ){
     ruta = '/login/provider/clients-filter'
    this.router.navigate([ruta])
  }
  console.log( "path de user " +  this.data.perfil  + "  es: " + ruta  )

}


}
