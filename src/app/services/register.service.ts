import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  URL_SERVICIOS = 'http://64.227.31.7:3000';

constructor(public http:HttpClient) { }

register(body){
  return this.http.post(`${this.URL_SERVICIOS}/user`,body);
}

}
