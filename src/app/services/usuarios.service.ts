import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
URL_SERVICIOS = "http://64.227.31.7:3000";

constructor(public http:HttpClient) { }

obtenerUsuarios(){
  return this.http.get(`${this.URL_SERVICIOS}/user`);
}

borrarUsuario(id){
  return this.http.delete(`${this.URL_SERVICIOS}/user/${id}`);
}

}
