import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import {LoginService} from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  FormRegister;

  constructor(public fb:FormBuilder,public router:Router, public loginService: LoginService) {

    this.FormRegister = this.fb.group( {

      email: ['', [Validators.required, Validators.email, Validators.maxLength(50) ] ],
      password: [ '',  [ Validators.required, Validators.minLength(8) ] ],

   })
  }

  ngOnInit() {
  }

  users = [
    {email:'admin@test.com',password:'12345678',role:'admin'},
    {email:'cliente@test.com',password:'12345678',role:'client'},
    {email:'proveedor@test.com',password:'12345678',role:'provider'}
  ]

  invalidCredentials = false;

  public login (){

    console.log( "LOGIN CLICK" )

    let body = this.FormRegister.value;
    console.log( body )

let user = body.email ;

     this.loginService.login(body).subscribe((res:any)=>{
     console.log( "res back" )

     console.log( res )
      if(res.ok == false){
         this.invalidCredentials = true;
       }else{
         localStorage.setItem('id',res.id);
         localStorage.setItem('role',res.role)
         switch (res.role) {
           case 'admin':
               this.router.navigate(['/login/admin/home'])
             break;
           case 'client':
               this.router.navigate(['home-client'])
             break;
           case 'provider':
               this.router.navigate(['/home-provider'])
             break;
           default:
             break;
         }
       }
    })

   /* login sin back
      let userLog = this.users.find((user)=>{
      return ( user.email == body.email && user.password == body.password)
    })


    if(userLog){
      localStorage.setItem('role',userLog.role)
      switch (userLog.role) {
        case 'admin':
            this.router.navigate(['/login/admin/home'])
          break;
        case 'client':
            this.router.navigate(['home-client'])
          break;
        case 'provider':
            this.router.navigate(['/home-provider'])
          break;
        default:
          break;
      }
    }else{
      console.log('user incorrecto')
    }*/

    


  }


}
