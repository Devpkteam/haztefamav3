import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatSelectModule,
  MatTabsModule,
  MatFormFieldModule,
  MatCardModule,
  MatDividerModule,
  MatCheckboxModule,
  MatIconModule,
  MatPaginatorModule,
  MatSortModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTableModule,
  MatInputModule,
  MatDialogModule,
  MatSnackBarModule
} from '@angular/material';

const materialModules = [
  MatButtonModule,
  MatSelectModule,
  MatTabsModule,
  MatFormFieldModule,
  MatCardModule,
  MatInputModule,
  MatDividerModule,
  MatCheckboxModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatDialogModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatInputModule,
  MatDialogModule,
  MatSnackBarModule
]

@NgModule({
  imports: [
    materialModules,
  ],
  exports:[
    materialModules
  ]
})
export class MaterialModule { }
