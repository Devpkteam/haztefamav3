import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './pages/layouts/admin-layout/admin-layout.component';
//Home
import { HomeComponent } from './home/home.component';
import { HomeProviderComponent } from './pages/provider-components/home-provider/home-provider.component'

// Login y registro
import { RegistroClienteComponent } from './pages/registros/registro-cliente/registro-cliente.component';
import { LoginComponent } from './login/login/login.component';

//import { ClientHomeComponent } from './pages/client/client-home/client-home.component';
import { ClientHomeComponent } from './pages/client-home/client-home.component';


const routes: Routes =[
  { path: 'home-client',  component: ClientHomeComponent, pathMatch: 'full' },
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
  },
  { path: 'register',
    component: RegistroClienteComponent,
    pathMatch: 'full',
  },
  { path: 'log',
    component: LoginComponent,
    pathMatch: 'full',
  },
  { path: 'home-provider',
    component: HomeProviderComponent,
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: AdminLayoutComponent,
    children: [
        {
      path: '',
      loadChildren: './pages/layouts/admin-layout/admin-layout.module#AdminLayoutModule'
  }]},
  {
    path: '**',
    redirectTo: 'dashboard'
  }



];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
