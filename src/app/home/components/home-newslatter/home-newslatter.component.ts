import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-home-newslatter',
  templateUrl: './home-newslatter.component.html',
  styleUrls: ['./home-newslatter.component.scss']
})
export class HomeNewslatterComponent implements OnInit {

  newsForm: FormGroup

  constructor(
    public form:FormBuilder,
  ) { 
    this.newsForm = form.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
    });
  }

  ngOnInit() {
  }

  suscribe(){
    console.log(this.newsForm.value);
  }

}
