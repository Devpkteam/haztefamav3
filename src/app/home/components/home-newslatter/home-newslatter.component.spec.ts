import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeNewslatterComponent } from './home-newslatter.component';

describe('HomeNewslatterComponent', () => {
  let component: HomeNewslatterComponent;
  let fixture: ComponentFixture<HomeNewslatterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeNewslatterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeNewslatterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
