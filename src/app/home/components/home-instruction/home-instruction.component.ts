import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-home-instruction',
  templateUrl: './home-instruction.component.html',
  styleUrls: ['./home-instruction.component.scss']
})
export class HomeInstructionComponent implements OnInit {

  
  constructor(private sanitizer: DomSanitizer) { }
  
  urlSinProcesar = "https://www.youtube.com/embed/KW0b_3cTqPA";
  urlSaneada = this.sanitizer.bypassSecurityTrustResourceUrl(this.urlSinProcesar);
  
  ngOnInit() {
  }

}
