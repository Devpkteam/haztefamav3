import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeInstructionComponent } from './home-instruction.component';

describe('HomeInstructionComponent', () => {
  let component: HomeInstructionComponent;
  let fixture: ComponentFixture<HomeInstructionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeInstructionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeInstructionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
